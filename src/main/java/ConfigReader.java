import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConfigReader {
    public static final Logger logger = Logger.getLogger(ConfigReader.class.getName());

    private static ConfigReader ourInstance = new ConfigReader();

    private static Map<String, String> config;

    private ConfigReader() {
        config = initConfig("youtrackemail.properties");
    }

    public static ConfigReader getInstance() {
        return ourInstance;
    }

    public static String getPropertyByName(String name) {
        return config.get(name);
    }

    private static Map<String, String> initConfig(String configName) {
        Properties props = readPropertiesFromFile(configName);
        Map<String, String> result = new HashMap<>(props.size());
        for (String key : props.stringPropertyNames()) {
            result.put(key, props.getProperty(key));
        }
        return result;
    }

    private static Properties readPropertiesFromFile(String configName) {
        Properties configProps = new Properties();
        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream fis = classloader.getResourceAsStream(configName);
            configProps.load(fis);
            fis.close();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "ERROR {0}", e.getMessage());
        } catch (Exception e) {
            logger.log(Level.SEVERE, "ERROR {0}", e.getMessage());
        }
        return configProps;
    }
}
