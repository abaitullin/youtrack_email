import entity.Item;
import entity.JsonItems;
import entity.YTrackIssue;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main {
    public static final Logger logger = Logger.getLogger(Main.class.getName());
    private static EmailManager emailManager;
    private static YouTrackManager youTrackManager;

    public static void main(String[] args) {
        //System.out.println("Start");
        //ConfigReader.getInstance();
        try {
            InputStream is = Main.class.getClassLoader().getResourceAsStream("logging.properties");
            LogManager.getLogManager().readConfiguration(is);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        final String user = ConfigReader.getPropertyByName("email.user");
        final String pass = ConfigReader.getPropertyByName("email.pass");
        final long timeout = Long.valueOf(ConfigReader.getPropertyByName("email.check.timeout"));
        final String host = ConfigReader.getPropertyByName("server.imap");
        final String saveDirectory = ConfigReader.getPropertyByName("attachment.dir");
        final String folderMain = ConfigReader.getPropertyByName("folder.main");
        final String folderProcessed = ConfigReader.getPropertyByName("folder.processed");
        final String youtrackEmailAddress = ConfigReader.getPropertyByName("email.target");

        final String youTrackPath = ConfigReader.getPropertyByName("youtrack.path");
        final String youTrackToken = ConfigReader.getPropertyByName("youtrack.token");

        logger.log(Level.INFO, "user={0}", user);
        logger.log(Level.INFO, "pass={0}", pass);
        logger.log(Level.INFO, "host={0}", host);
        logger.log(Level.INFO, "folderMain={0}", folderMain);
        logger.log(Level.INFO, "folderProcessed={0}", folderProcessed);
        logger.log(Level.INFO, "youtrackEmailAddress={0}", youtrackEmailAddress);
        youTrackManager = new YouTrackManager(youTrackPath, youTrackToken);
        //System.out.println(youTrackManager.checkJson());
        JsonItems jsonItems = youTrackManager.getProjects();
        //youTrackManager.attachFile("2-3137");
        //youTrackManager.createIssue(null);
        //System.out.println(youTrackManager.checkJson());

        emailManager = new EmailManager(user, pass, host, saveDirectory, youtrackEmailAddress);
        processEmail(folderMain, folderProcessed, timeout, jsonItems.getItems());

        logger.log(Level.INFO, "Finish");
    }

    public static void processEmail(String folderMain, String folderProcessed, long timeout, List<Item> projects) {
        if (!emailManager.initialize()) {
            logger.log(Level.SEVERE, "Error with initialization JavaMail");
            return;
        }
        //emailManager.printEmails(folderMain);

        boolean error = false;

        while (!error) {
            try {
                logger.log(Level.INFO, "Start checking email {0}", System.currentTimeMillis());
                List<YTrackIssue> list = emailManager.getNewEmails(folderMain, folderProcessed);
                for (YTrackIssue y : list) {
                    if (youTrackManager.createIssue(y, projects)) {
                        for (String filename : y.getAttachments()) {
                            error = !youTrackManager.attachFile(y.getIssueId(), filename);
                        }
                    } else {
                        error = true;
                    }
                }
                logger.log(Level.INFO, "Finish checking email {0}", System.currentTimeMillis());
                break;
                //Thread.sleep(timeout);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "ERROR {0}", e.getMessage());
                error = true;
            }
        }
    }
}

