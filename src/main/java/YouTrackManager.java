import entity.*;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import reader.ItemMessageBodyReader;
import reader.ItemsMessageBodyReader;
import reader.MyBeanMessageBodyWriter;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class YouTrackManager {
    public static final Logger logger = Logger.getLogger(YouTrackManager.class.getName());

    private String path;
    private String token;

    public YouTrackManager(String path, String token) {
        this.path = path;
        this.token = token;
    }

    public JsonItems getProjects() {

        Client client = ClientBuilder.newBuilder().register(ItemsMessageBodyReader.class).build();

        WebTarget webTarget = client.target(path);
        WebTarget resourceWebTarget = webTarget.path("admin/projects");
        WebTarget helloworldWebTargetWithQueryParam =
                resourceWebTarget.queryParam("fields", "id,name,shortName");

        Invocation.Builder invocationBuilder =
                helloworldWebTargetWithQueryParam.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("Authorization", token);
        invocationBuilder.header("Content-Type", "application/json");


        Response response = invocationBuilder.get();
        logger.log(Level.INFO, "response.getStatus() = {0}", response.getStatus());
        JsonItems items = response.readEntity(JsonItems.class);
        logger.log(Level.INFO, "items = {0}", items);
        return items;
    }

    public boolean createIssue(YTrackIssue yTrackIssue, List<Item> projects) {
        try {

            Client client = ClientBuilder.newBuilder()
                    .register(MyBeanMessageBodyWriter.class)
                    .register(ItemMessageBodyReader.class)
                    .build();

            WebTarget webTarget = client.target(path);
            WebTarget resourceWebTarget = webTarget.path("issues");

            Invocation.Builder invocationBuilder =
                    resourceWebTarget.request(MediaType.APPLICATION_JSON);
            invocationBuilder.header("Authorization", token);
            invocationBuilder.header("Content-Type", "application/json");

            Issue issue = new Issue();
            issue.setSummary(yTrackIssue.getTaskName());
            issue.setDescription(yTrackIssue.getDescription());

            boolean findProject = false;
            for (Item project : projects) {
                if (project.getShortName().toLowerCase().equals(yTrackIssue.getProjectName().toLowerCase())) {
                    issue.setProject(new Project(project.getId()));
                    findProject = true;
                    break;
                }
            }

            if (!findProject) {
                logger.log(Level.SEVERE, "(createIssue) Project not found. ShortName: {0}", yTrackIssue.getProjectName());
                return false;
            }

            Response response = invocationBuilder.post(Entity.json(issue));
            logger.log(Level.INFO, "response.getStatus() = {0}", response.getStatus());
            JsonItem item = response.readEntity(JsonItem.class);
            logger.log(Level.INFO, "json = {0}", item.getJson());
            logger.log(Level.INFO, "item = {0}", item);
            yTrackIssue.setIssueId(item.getItem().getId());
            if (item.getError() != null) {
                return false;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "(createIssue) Error with issue: {0}", yTrackIssue);
            logger.log(Level.SEVERE, "YouTrackManager.createIssue", e);
            return false;
        }
        return true;
    }

    public boolean attachFile(String issueId, String fileName) {
        try {
            Client client = ClientBuilder.newBuilder()
                    .register(MultiPartFeature.class)
                    .register(ItemsMessageBodyReader.class)
                    .build();

            WebTarget webTarget = client.target(path);
            WebTarget issueWebTarget = webTarget.path("issues");
            WebTarget thisIssueWebTarget = issueWebTarget.path(issueId);
            WebTarget attachThisIssueWebTarget = thisIssueWebTarget.path("attachments");

            Invocation.Builder invocationBuilder =
                    attachThisIssueWebTarget.request(MediaType.APPLICATION_JSON);
            invocationBuilder.header("Authorization", token);
            //invocationBuilder.header("Content-Type", "multipart/form-data");
            FileDataBodyPart filePart = new FileDataBodyPart("my_file", new File(fileName));
            FormDataMultiPart multipart = (FormDataMultiPart) new FormDataMultiPart()
                    //.field("foo", "bar")
                    .bodyPart(filePart);

            Response response = invocationBuilder.post(Entity.entity(multipart, MediaType.MULTIPART_FORM_DATA));
            logger.log(Level.INFO, "response.getStatus() = {0}", response.getStatus());
            JsonItems items = response.readEntity(JsonItems.class);
            logger.log(Level.INFO, "json = {0}", items.getJson());
            logger.log(Level.INFO, "item = {0}", items);
            if (items.getError() != null) {
                return false;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "(attachFile) Error with issue: {0}", issueId);
            logger.log(Level.SEVERE, "(attachFile) Error with file: {0}", fileName);
            logger.log(Level.SEVERE, "YouTrackManager.attachFile", e);
            return false;
        }
        return true;
    }
}
