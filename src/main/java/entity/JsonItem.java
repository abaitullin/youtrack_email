package entity;

public class JsonItem {
    Item item;
    String json;
    JsonError error;

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public JsonError getError() {
        return error;
    }

    public void setError(JsonError error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "JsonItem{" +
                "item=" + item +
                ", json='" + json + '\'' +
                ", error=" + error +
                '}';
    }
}
