package entity;

import java.util.List;

public class JsonItems {
    private List<Item> items;
    private JsonError error;
    String json;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public JsonError getError() {
        return error;
    }

    public void setError(JsonError error) {
        this.error = error;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    @Override
    public String toString() {
        return "Items{" +
                "items=" + items +
                ", error=" + error +
                ", json='" + json + '\'' +
                '}';
    }
}
