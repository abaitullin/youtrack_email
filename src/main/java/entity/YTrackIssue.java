package entity;

import java.util.List;

public class YTrackIssue {
    private String projectName;
    private String taskName;
    private String description;
    private List<String> attachments;
    private String issueId;

    public YTrackIssue(String projectName,
                       String taskName,
                       String description,
                       List<String> attachments,
                       String issueId) {
        this.projectName = projectName;
        this.taskName = taskName;
        this.description = description;
        this.attachments = attachments;
        this.issueId = issueId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<String> attachments) {
        this.attachments = attachments;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }
}
