package reader;

import entity.JsonItem;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ItemMessageBodyReader
        implements MessageBodyReader<JsonItem> {
    public static final Logger logger = Logger.getLogger(ItemMessageBodyReader.class.getName());

    @Override
    public boolean isReadable(Class<?> type, Type genericType,
                              Annotation[] annotations, MediaType mediaType) {
        return type == JsonItem.class;
    }

    @Override
    public JsonItem readFrom(Class<JsonItem> type,
                             Type genericType,
                             Annotation[] annotations, MediaType mediaType,
                             MultivaluedMap<String, String> httpHeaders,
                             InputStream entityStream)
            throws IOException, WebApplicationException {
        JsonItem item = new JsonItem();
        try {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(entityStream))) {
                String result = br.lines().collect(Collectors.joining("\n"));
                item.setJson(result);
                if (result.contains("error")) {
                    item.setError(ReaderUtils.getJsonError(result));
                    return item;
                }

                JSONObject responseJsonObject = (JSONObject) JSONValue.parseWithException(result);

                item.setItem(ReaderUtils.getValue(responseJsonObject));
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "ERROR {0}", e.getMessage());
        }
        return item;
    }
}
