package reader;

import entity.Item;
import entity.JsonItems;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ItemsMessageBodyReader
        implements MessageBodyReader<JsonItems> {
    public static final Logger logger = Logger.getLogger(ItemsMessageBodyReader.class.getName());

    @Override
    public boolean isReadable(Class<?> type, Type genericType,
                              Annotation[] annotations, MediaType mediaType) {
        return type == JsonItems.class;
    }

    @Override
    public JsonItems readFrom(Class<JsonItems> type,
                              Type genericType,
                              Annotation[] annotations, MediaType mediaType,
                              MultivaluedMap<String, String> httpHeaders,
                              InputStream entityStream)
            throws WebApplicationException {
        JsonItems items = new JsonItems();
        try {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(entityStream))) {
                String result = br.lines().collect(Collectors.joining("\n"));
                items.setJson(result);
                if (result.contains("error")) {
                    items.setError(ReaderUtils.getJsonError(result));
                    return items;
                }
                List<Item> itemList = new ArrayList<>();
                JSONArray responseJsonObject = (JSONArray) JSONValue.parseWithException(result);
                for (Object obj : responseJsonObject) {
                    Item item = ReaderUtils.getValue((JSONObject) obj);
                    itemList.add(item);
                }

                items.setItems(itemList);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "ERROR {0}", e.getMessage());
        }
        return items;
    }

}
