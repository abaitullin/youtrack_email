package reader;

import entity.Issue;
import org.json.simple.JSONObject;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

@Produces("application/json")
public class MyBeanMessageBodyWriter implements MessageBodyWriter<Issue> {

    @Override
    public boolean isWriteable(Class<?> type, Type genericType,
                               Annotation[] annotations, MediaType mediaType) {
        return type == Issue.class;
    }

    @Override
    public long getSize(Issue myBean, Class<?> type, Type genericType,
                        Annotation[] annotations, MediaType mediaType) {
        // deprecated by JAX-RS 2.0 and ignored by Jersey runtime
        return -1;
    }

    @Override
    public void writeTo(Issue myBean,
                        Class<?> type,
                        Type genericType,
                        Annotation[] annotations,
                        MediaType mediaType,
                        MultivaluedMap<String, Object> httpHeaders,
                        OutputStream entityStream)
            throws IOException, WebApplicationException {
        JSONObject obj = new JSONObject();
        JSONObject project = new JSONObject();
        project.put("id", myBean.getProject().getId());
        obj.put("project", project);
        obj.put("summary", myBean.getSummary());
        obj.put("description", myBean.getDescription());

        entityStream.write(obj.toJSONString().getBytes("UTF-8"));
    }
}
