package reader;

import entity.Item;
import entity.JsonError;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ReaderUtils {
    public static final Logger logger = Logger.getLogger(ReaderUtils.class.getName());

    public static Item getValue(JSONObject responseJsonObject) {
        Item item = new Item();
        String id = (String) responseJsonObject.get("id");
        String shortName = (String) responseJsonObject.get("shortName");
        String name = (String) responseJsonObject.get("name");
        String type = (String) responseJsonObject.get("$type");
        item.setId(id);
        item.setType(type);
        item.setShortName(shortName);
        item.setName(name);
        return item;
    }

    public static JsonError getJsonError(String json) {
        try {
            JSONObject responseJsonObject = (JSONObject) JSONValue.parseWithException(json);

            String error = (String) responseJsonObject.get("error");
            String error_description = (String) responseJsonObject.get("error_description");
            return new JsonError(error, error_description);
        } catch (org.json.simple.parser.ParseException e) {
            logger.log(Level.SEVERE, "ERROR {0}", e.getMessage());
        }
        return null;
    }
}
